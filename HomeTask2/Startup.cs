﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace HomeTask2
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.Use(async (context, next) =>
            {
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                await next.Invoke();
                stopwatch.Stop();
                await context.Response.WriteAsync($"time taken: {stopwatch.ElapsedMilliseconds} milliseconds\r\n");
            });

            app.UseMiddleware<TimeMiddleware>();

            app.Use(async (context, next) =>
            {
                Thread.Sleep(200);
                await next.Invoke();
            });
        }
    }

    public class TimeMiddleware
    {
        private readonly RequestDelegate _next;

        public TimeMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            await _next.Invoke(context);
            stopwatch.Stop();
            await context.Response.WriteAsync($"time taken: {stopwatch.ElapsedMilliseconds} milliseconds\r\n");
        }
    }
}
